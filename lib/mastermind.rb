class Code
  attr_reader :pegs

  PEGS = {"r" => :red, "o" => :orange, "y" => :yellow, "g" => :green,
          "b" => :blue, "p" => :purple }

  def initialize(pegs)
    @pegs = pegs
  end

  def self.random
    result = []
    4.times { result << PEGS.values.sample }
    Code.new(result)
  end

  def self.parse(input)
    pegs = input.downcase.chars
    raise unless pegs.all? { |char| PEGS.keys.include?(char) }
    pegs = pegs.map { |char| PEGS[char] }
    Code.new(pegs)
  end

  def exact_matches(code2)
    return 4 if self.pegs.==(code2.pegs)

    count = 0
    self.pegs.each_index do |idx|
      count += 1 if self.pegs[idx] == code2.pegs[idx]
    end

    count
  end

  require 'byebug'

  def near_matches(code2)
    code2_dup = code2.pegs.dup
    self_dup = self.pegs.dup
    self_pegs, code2_pegs = delete_exact_matches(code2_dup, self_dup)
    count = 0

    self_pegs.each_index do |idx1|
      code2_pegs.each_index do |idx2|
        if self_pegs[idx1] == code2_pegs[idx2]
          count += 1
          code2_pegs.delete_at(idx2)
          break
        end
      end
    end

    count
  end

  def [](idx)
    @pegs[idx]
  end

  def ==(code2)
    return false unless code2.instance_of?(Code)
    @pegs == code2.pegs
  end

  private

  def delete_exact_matches(pegs1, pegs2)
    pegs1.each_index do |i|
      if pegs1[i] == pegs2[i]
        pegs1.delete_at(i)
        pegs2.delete_at(i)
      end
    end

    [pegs1, pegs2]
  end
end

class Game
  attr_reader :secret_code

  def initialize(secret_code = nil)
    @secret_code = secret_code || Code.random
  end

  def get_guess
    puts("Place your guess (a string of 4 letters, no spaces)")
    guess = gets.chomp
    @guess = Code.parse(guess)
  end

  def display_matches
    near_count = @secret_code.near_matches(@guess)
    exact_count = @secret_code.exact_matches(@guess)

    if near_count == 1
      puts("There is #{near_count} near match")
    else
      puts("There are #{near_count} near matches")
    end

    if exact_count == 1
      puts("There is #{exact_count} exact match")
    else
      puts("There are #{exact_count} exact matches")
    end
  end

  def play
    until @secret_code == @guess
      get_guess
      break if @secret_code == @guess
      display_matches
    end

    puts("You win. <unenthusiastic cheering>")
  end
end
